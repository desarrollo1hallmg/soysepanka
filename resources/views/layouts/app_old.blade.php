<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('old/js/app.js') }}" defer></script>
    <!--<script src="https://unpkg.com/aos@next/dist/aos.js"></script>-->
    <!--<script>
        AOS.init();
    </script>-->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

    <!--scroll animations-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <!-- Styles -->
    <link href="{{ asset('old/css/app.css') }}" rel="stylesheet">
</head>
<body>
        <?php function activeMenu($url){
            return request()->is($url) ? 'bg-login' : '';
        }?>
    <div id="app" class="{{activeMenu('login')}} animated fadeIn">
        <header class="header">
                
            <div class="container">
                <div class="row">
                    <div class="col">
                        <a class="navbar-brand font-weight-bold" href="/welcome">
                            Soysepanka
                        </a>
                    </div>
                    <div class="col">
                            <ul class="nav d-flex justify-content-end font-weight-bold">
                                    <!-- Authentication Links -->
                                    @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                        @if (Route::has('register'))
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('register') }}">{{ __('Registro') }}</a>
                                            </li>
                                        @endif
                                    @else
                                        {{--<li class="nav-item dropdown">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>
                    
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>
                    
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                     @csrf
                                                </form>
                                            </div>
                                        </li>--}}

                                        <a class="" href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                        </form>
                                    @endguest
                                </ul>
                    </div>
                </div>
            </div>

            <!-- Right Side Of Navbar -->
            
        </header>
       
        <main class="py-5">
            @yield('content')
        </main>
    </div>
</body>
</html>