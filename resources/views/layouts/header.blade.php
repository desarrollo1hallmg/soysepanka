  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">{{ config('app.name', 'Soysepanka') }}</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- Modulos Menu -->
         <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The icon in the navbar-->
              <i class="fa fa-th"></i>
            </a>
            <ul class="dropdown-menu">
             <div class="row">

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/comunicacion_interna.svg") }}"  class="img-circle" alt="User Image">Comunicación
                  </a>
                </div>

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/gestion_de_vacantes.svg") }}"  class="img-circle" alt="User Image">Vacantes
                  </a>
                </div>

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/evaluacion_del_desempeno.svg") }}"  class="img-circle" alt="User Image">Desempeño
                  </a>
                </div>

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/resultados.svg") }}"  class="img-circle" alt="User Image">Resultados
                  </a>
                </div>

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/clima.svg") }}" class="img-circle" alt="User Image">Clima
                  </a>
                </div>

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/control_de_incidencias.svg") }}"  class="img-circle" alt="User Image">Incidencias
                  </a>
                </div>

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/reconocimiento_al_personal.svg") }}"  class="img-circle" alt="User Image">Reconocimiento
                  </a>
                </div>

                <div class="col-xs-4" align="center">
                  <a href="#">
                    <img src="{{ asset("img/modulos/capacitacion_en_linea.svg") }}"  class="img-circle" alt="User Image">Capacitación
                  </a>
                </div>

              </div>
            </ul>
          </li>

          <!-- Mensajes Menu -->
          <!--<li class="dropdown messages-menu">-->
            <!-- Menu Toggle Button -->
            <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
              <!-- The user image in the navbar-->
              <!--<i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Mensajes</li>
              <li>
                <ul class="menu">
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
                      </div>
                      <h4>Equipo de Soporte
                        <small>
                          <i class="fa fa-clock-o"></i> 5 min
                        </small>
                      </h4>
                      <p>Seguimiento de solicitud</p>
                    </a>
                  </li>
                  
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
                      </div>
                      <h4>Equipo de Soporte
                        <small>
                          <i class="fa fa-clock-o"></i> 5 min
                        </small>
                      </h4>
                      <p>Seguimiento de solicitud</p>
                    </a>
                  </li>
                  
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
                      </div>
                      <h4>Equipo de Soporte
                        <small>
                          <i class="fa fa-clock-o"></i> 5 min
                        </small>
                      </h4>
                      <p>Seguimiento de solicitud</p>
                    </a>
                  </li>

                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
                      </div>
                      <h4>Equipo de Soporte
                        <small>
                          <i class="fa fa-clock-o"></i> 5 min
                        </small>
                      </h4>
                      <p>Seguimiento de solicitud</p>
                    </a>
                  </li>
                  
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
                      </div>
                      <h4>Equipo de Soporte
                        <small>
                          <i class="fa fa-clock-o"></i> 5 min
                        </small>
                      </h4>
                      <p>Seguimiento de solicitud</p>
                    </a>
                  </li>
                  
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
                      </div>
                      <h4>Equipo de Soporte
                        <small>
                          <i class="fa fa-clock-o"></i> 5 min
                        </small>
                      </h4>
                      <p>Seguimiento de solicitud</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer">
                <a href="#">Ver todos los mensajes</a>
              </li>
            </ul>
          </li>-->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Notificaciones</li>
              <li>
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> Notificacion 1
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> Notificacion 2
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-green"></i> Notificacion 3
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-black"></i> Notificacion 4
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-blue"></i> Notificacion 5
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>

          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <i class="fa fa-gears"></i>
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->username }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">

                <p>
                  Hello {{ Auth::user()->username }}
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
               @if (Auth::guest())
                  <div class="pull-left">
                    <a href="{{ route('login') }}" class="btn btn-default btn-flat">Login</a>
                  </div>
               @else
                 <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                  </div>
                 <div class="pull-right">
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    Logout
                    </a>
                 </div>
                @endif
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>
   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      {{ csrf_field() }}
   </form>