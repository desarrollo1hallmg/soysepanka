@extends('dashboard.base')
@section('action-content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <!--left column-->
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Noticias Recientes</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
        </div>
        <div class="box-body" style>
          <ul class="products-list product-list-in-box">
            <li class="item">
              <div class="product-img">
                <img src="{{ asset("/img/anuncios/anuncio2.jpg") }}" alt="Product Page">
              </div>
              <div class="products-info">
                <a href="#" class="product-title">
                  Anuncio 1
                  <span class="label label-warning pull-right">Hoy</span>
                </a>
                <span class="product-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi faucibus malesuada aliquam.</span>
              </div>
            </li>
            <li class="item">
              <div class="product-img">
                <img src="{{ asset("/img/anuncios/anuncio2.jpg") }}" alt="Product Page">
              </div>
              <div class="products-info">
                <a href="#" class="product-title">
                  Anuncio 1
                  <span class="label label-warning pull-right">Hoy</span>
                </a>
                <span class="product-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi faucibus malesuada aliquam.</span>
              </div>
            </li>
            <li class="item">
              <div class="product-img">
                <img src="{{ asset("/img/anuncios/anuncio2.jpg") }}" alt="Product Page">
              </div>
              <div class="products-info">
                <a href="#" class="product-title">
                  Anuncio 1
                  <span class="label label-warning pull-right">Hoy</span>
                </a>
                <span class="product-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi faucibus malesuada aliquam.</span>
              </div>
            </li>
            <li class="item">
              <div class="product-img">
                <img src="{{ asset("/img/anuncios/anuncio2.jpg") }}" alt="Product Page">
              </div>
              <div class="products-info">
                <a href="#" class="product-title">
                  Anuncio 1
                  <span class="label label-warning pull-right">Hoy</span>
                </a>
                <span class="product-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi faucibus malesuada aliquam.</span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <!--end left column-->

    <!--right column-->
    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Mis Métricas</h3>
          <div class="box-tools">
            <ul class="pagination pagination-sm no-margin pull-right">
              <li>
                <a href="#"><<</a>
              </li>
              <li>
                <a href="#">1</a>
              </li>
              <li>
                <a href="#">2</a>
              </li>
              <li>
                <a href="#">3</a>
              </li>
              <li>
                <a href="#">>></a>
              </li>
            </ul>
          </div>
        </div>

        <div class="box-body no-padding">
          <table class="table">
            <tbody>
              <tr>
                <th style="width: 10px">#</th>
                <th>Objetivo</th>
                <th>Progreso</th>
                <th style="width: 40px">%</th>
              </tr>
              
              <tr>
                <td>1</td>
                <td>Proyecto Agile</td>
                <td>
                  <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                  </div>
                </td>
                <td>
                  <span class="badge bg-red">55%</span>
                </td>
              </tr>
              
              <tr>
                <td>2</td>
                <td>Unificar Seguridad</td>
                <td>
                  <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-yellow" style="width: 77%"></div>
                  </div>
                </td>
                <td>
                  <span class="badge bg-yellow">77%</span>
                </td>
              </tr>
              
              <tr>
                <td>3</td>
                <td>Proyecto Cliente B</td>
                <td>
                  <div class="progress progress-xs progress-striped active">
                    <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                  </div>
                </td>
                <td>
                  <span class="badge bg-light-blue">30%</span>
                </td>
              </tr>

              <tr>
                <td>4</td>
                <td>Conformar Equipo de trabajo</td>
                <td>
                  <div class="progress progress-xs progress-striped active">
                    <div class="progress-bar progress-bar-success" style="width: 90%"></div>
                  </div>
                </td>
                <td>
                  <span class="badge bg-green">90%</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--end right column-->
  </div>
  
  <div class="row">

    <div class="col-md-6">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Rotacion de personal</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="barChart" style="height: 230px; width: 510px;" width="510" height="230"></canvas>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Cumpleaños del Mes</h3>
          <div class="box-tools pull-right">
            <span class="label label-danger">8 Cumpleañeros</span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
        </div>
        <div class="box-body no-padding">
          <ul class="users-list clearfix">
            <li>
              <img src="{{ asset ("/img/usuarios/user1.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Armando Perez</a>
              <span class="users-list-date">Ayer</span>
            </li>
            <li>
              <img src="{{ asset ("/img/usuarios/user2.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Victor Gonzalez</a>
              <span class="users-list-date">Hoy</span>
            </li>
            <li>
              <img src="{{ asset ("/img/usuarios/user3.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Eduardo</a>
              <span class="users-list-date">Mañana</span>
            </li>
            <li>
              <img src="{{ asset ("/img/usuarios/user4.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Daniel</a>
              <span class="users-list-date">Lunes</span>
            </li>
            <li>
              <img src="{{ asset ("/img/usuarios/user5.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Francisco</a>
              <span class="users-list-date">Martes</span>
            </li>
            <li>
              <img src="{{ asset ("/img/usuarios/user6.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Lizette</a>
              <span class="users-list-date">Jueves</span>
            </li>
            <li>
              <img src="{{ asset ("/img/usuarios/user7.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Norah</a>
              <span class="users-list-date">Sabado</span>
            </li>
            <li>
              <img src="{{ asset ("/img/usuarios/user8.png") }}" alt="User Image">
              <a href="#" class="users-list-name">Juan</a>
              <span class="users-list-date">Lunes</span>
            </li>
          </ul>
        </div>
        <div class="box-footer text-center">
          <a href="#" class="uppercase">Ver Todos los Cumpleañeros</a>
        </div>
      </div>
    </div>

  </div>

  <div class="row">

    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Géneros</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">
              <div class="chart-responsive">
                <canvas id="pieChart" height="160" width="205" style="width: 205px; height: 160px;"></canvas>
              </div>
            </div>
            <div class="col-md-4">
              <ul class="chart-legend clearfix">
                <li>
                  <i class="fa fa-circle-o text-red"></i>Hombres
                </li>
                <li>
                  <i class="fa fa-circle-o text-blue"></i>Mujeres
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="box-footer no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li>
              <a href="#">USA
                <span class="pull-right text-red">
                  <i class="fa fa-angle-down"></i>12%
                </span>
              </a>
            </li>
            <li>
              <a href="#">India
                <span class="pull-right text-green">
                  <i class="fa fa-angle-up"></i>87%
                </span>
              </a>
            </li>
            <li>
              <a href="#">China
                <span class="pull-right text-yellow">
                  <i class="fa fa-angle-left"></i>0%
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-widget">
        <div class="box-header with-border">
          <div class="user-block">
            <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
            <span class="username">
              <a href="#">Eric Vargas R.</a>
            </span>
            <span class="description">Publicado - 7:30 PM Hoy</span>
          </div>
          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title data-original-title="Marcar como leído">
              <i class="fa fa-circle-o"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <img src="{{ asset("/img/anuncios/anuncio.jpg") }}" class="img-responsive pad" alt="Photo">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi faucibus malesuada aliquam. In hac habitasse platea dictumst. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent luctus massa ac eros posuere, ac placerat turpis efficitur. In sem tortor, vulputate eget nisi in, tincidunt tempor ex. Quisque vestibulum dictum vulputate. Nulla facilisi. Sed at diam sit amet eros tempus blandit. Aenean id aliquet arcu. Integer et eleifend ipsum. Proin eget semper dui. Integer non semper ex. Nulla facilisi. Suspendisse non ligula aliquam, laoreet turpis vitae, fermentum leo. Nulla id ligula eget elit gravida accumsan. Vestibulum metus leo, vehicula finibus cursus et, consequat ut lectus.</p>
          <button type="button" class="btn btn-default btn-xs">
            <i class="fa fa-share"></i>
          </button>
          <button type="button" class="btn btn-default btn-xs">
            <i class="fa fa-thumbs-o-up"></i>
          </button>
          <span class="pull-right text-muted">127 Me gusta - 3 comentarios</span>
        </div>
        <div class="box-footer box-comments">
          
        </div>
        <div class="box-footer">
          
        </div>
      </div>
    </div>

  </div>

</section>
<script  src="{{ asset ("/bower_components/AdminLTE/plugins/chartjs/Chart.js") }}" type="text/javascript"></script>
<script>
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */
    var areaChartData = {
      labels  : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'],
      datasets: [
        {
          label               : 'Empresa',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label               : 'Departamento',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [28, 48, 40, 19, 86, 27, 90]
        },
        {
          label               : 'Puesto',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [11, 23, 65, 12, 47, 70, 33]
        }
      ]
    }
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = document.getElementById("barChart").getContext('2d');
    var barChart                         = new Chart(barChartCanvas);
    var barChartData                     = areaChartData;
    barChartData.datasets[1].fillColor   = '#00a65a';
    barChartData.datasets[1].strokeColor = '#00a65a';
    barChartData.datasets[1].pointColor  = '#00a65a';
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions)

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = document.getElementById("pieChart").getContext('2d');
    var pieChart       = new Chart(pieChartCanvas);
    var PieData        = [
      {
        value    : 700,
        color    : '#f56954',
        highlight: '#f56954',
        label    : 'Chrome'
      },
      {
        value    : 500,
        color    : '#00a65a',
        highlight: '#00a65a',
        label    : 'IE'
      }
    ];
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

</script>
@endsection