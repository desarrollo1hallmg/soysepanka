@extends('layouts.app_old')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card log-cd-bg animated bounceIn">
                <div class="card-header log-ch-bg text-center">{{ __('Login') }}</div>

                <div class="card-body log-cb-bg">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group my-4">
                            {{--<label for="email" class="col-md-4 offset-md-2 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>--}}

                            <div class="col-md-8 offset-md-2">
                                <input id="email" type="email" placeholder="User E-mail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            {{--<label for="password" class="col-md-4 offset-md-2 col-form-label text-md-left">{{ __('Password') }}</label>--}}

                            <div class="col-md-8 offset-md-2">
                                <input id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--<div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>--}}

                        <div class="form-group row mt-5">
                            <div class="col-md-8 offset-0 offset-md-2">
                                <button type="submit" class="btn col-md-12 btn-primary font-weight-bold">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link font-weight-bold" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection